DOCKER             = docker
DOCKER_COMPOSE     = docker-compose

EXEC_RUST          = $(DOCKER_COMPOSE) run --rm rust

EXEC_CARGO         = $(EXEC_RUST) cargo
EXEC_SPL_TOKEN         = $(EXEC_RUST) spl-token
EXEC_SOLANA        = $(EXEC_RUST) solana
EXEC_SOLANA_KEYGEN = $(EXEC_RUST) solana-keygen

##
##PROJECT
##-------

build: ## Build docker image
	@$(DOCKER_COMPOSE) pull --parallel
	$(DOCKER_COMPOSE) build --pull

kill: ## Remove docker image
	$(DOCKER) rmi rust

reset: ## Kill and Build docker image
reset: kill build

##
##UTILS
##-------

bash: ## Access docker image with bash
bash:
	$(EXEC_RUST) bash

##
##VERSION
##-------

solana-version: ## Display Solana version
solana-version:
	$(EXEC_SOLANA) --version

##
##SOLANA
##-------

# solana-balance:
# solana-balance:
#     $(EXEC_SOLANA) balance

# solana-keygen:
# solana-keygen:
#     $(EXEC_SOLANA_KEYGEN) new -o /root/.config/solana/id.json
#
# solana-devnet:
# solana-devnet:
#     $(EXEC_SOLANA) config set --url https://api.devnet.solana.com
#
# solana-airdrop:
# solana-airdrop:
#     $(EXEC_SOLANA) airdrop $(airdrop_number)

# EqG7fQC5AmsqmUDYdRR3ZtEubLBfurVmHPMLKUKdqrra pub key

##
##CARGO
##-------

# spl-token-create:
# spl-token-create:
#     $(EXEC_SPL_TOKEN) create-token
#
# spl-token-create-account:
# spl-token-create-account:
#     $(EXEC_SPL_TOKEN) create-account BqjpabQB3XxHcye3U2NvbFR3rNeL8GyWMBnqETiCWCqB
#
# spl-token-supply:
# spl-token-supply:
#     $(EXEC_SPL_TOKEN) supply BqjpabQB3XxHcye3U2NvbFR3rNeL8GyWMBnqETiCWCqB
#
# spl-token-mint:
# spl-token-mint:
#     $(EXEC_SPL_TOKEN) mint BqjpabQB3XxHcye3U2NvbFR3rNeL8GyWMBnqETiCWCqB 1000000
#
# spl-token authorize BqjpabQB3XxHcye3U2NvbFR3rNeL8GyWMBnqETiCWCqB mint --disable

##
##DOCS
##-------

.DEFAULT_GOAL := doc
doc: ## List commands available in Makefile
doc:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
